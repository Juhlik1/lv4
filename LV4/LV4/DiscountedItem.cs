﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4
{
    class DiscountedItem : RentableDecorator
    {
        private double Discount ;
        public DiscountedItem(IRentable rentable,double discount) : base(rentable) 
        {
            Discount = discount;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice()*0.01*Discount;
        }
        public override String Description
        {
            get
            {
                return "now at"+ Discount +"% off!" + base.Description;
            }
        }
    }
}

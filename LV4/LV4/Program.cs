﻿using System;
using System.Collections.Generic;

namespace LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zad 2.
            Analyzer3rdParty Analayz = new Analyzer3rdParty();
            Dataset FirstDataset = new Dataset(@"C:\Users\Jole\Desktop\Book1");
            Adapter FirstAdapter = new Adapter(Analayz);
            double[] DataArray =FirstAdapter.CalculateAveragePerColumn(FirstDataset);
            Console.WriteLine(string.Join("\n", DataArray));
            double[] DataArray2 =FirstAdapter.CalculateAveragePerRow(FirstDataset);
            Console.WriteLine(string.Join("\n", DataArray2));
            //Zad 3.
            List<IRentable> rentables = new List<IRentable>();
            Book book1 = new Book("Knjiga1");
            Video video1 = new Video("Video1");
            rentables.Add(book1);
            rentables.Add(video1);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(rentables);
            printer.PrintTotalPrice(rentables);
            //Zad 4.
            Book book2 = new Book("Knjiga1");
            Video video2 = new Video("Video1");
            HotItem HotBook = new HotItem(book2);
            HotItem HotVideo = new HotItem(video2);
            rentables.Add(HotBook);
            rentables.Add(HotVideo);
            printer.DisplayItems(rentables);
            printer.PrintTotalPrice(rentables);
            //Zad 5.
            List<IRentable> flashSale = new List<IRentable>();
            DiscountedItem discountedItem1 = new DiscountedItem(book1, 25);
            DiscountedItem discountedItem2 = new DiscountedItem(video1, 25);
            flashSale.Add(discountedItem1);
            flashSale.Add(discountedItem2);
            printer.DisplayItems(flashSale);
            printer.PrintTotalPrice(flashSale);


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV4
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int rowCount = data.Length;
            int ColumnCount = data[0].Length;
            double ColumnSum=0;
            double[] results = new double[ColumnCount];
            for (int i = 0; i < ColumnCount; i++)
            {
                ColumnSum = 0;
                for(int j=0;j< rowCount; j++)
                {
                    ColumnSum = ColumnSum + data[j][i];
                }
                results[i] = ColumnSum/rowCount;
 
            }
            return results;
        }

    }
}

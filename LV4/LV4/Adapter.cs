﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4
{
    class Adapter
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> dataList = dataset.GetData();
            int rows = dataList.Count;
            int CounterColum = 0;
            int MaxNumberOfColum = 0;
            int i = 0;int j = 0;
            foreach (List<double> list in dataList)
            {
                CounterColum = 0;
                foreach (double value in list)
                {
                    CounterColum++;
                }
                if (CounterColum > MaxNumberOfColum)
                {
                    MaxNumberOfColum = CounterColum;
                }
  
            }
            double[][] dataArray = new double[rows][];
            for(int k =0; k < rows; k++)
            {
                
                dataArray[k] = new double[MaxNumberOfColum];
            }
  
            foreach (List<double> list in dataList)
            {
                
                foreach (double value in list)
                {
                    dataArray[i][j] = value;
                    j++;
                }
                i++;
            }
            return dataArray;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }

    }
}

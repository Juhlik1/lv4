﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Die
    {
        private int numberOfSides;
        //private Random randomGenerator;
        private RandomGenerator RandomInt;
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.RandomInt = new RandomGenerator();
        }
        // public Die(int numberOfSides)
        // {
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        // }
        // public Die(int numberOfSides,Random RanomGenerator)
        // {
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = RanomGenerator;
        // }
        public int Roll()
        {
            int rolledNumber = RandomInt.NextInt(1, numberOfSides + 1);
        //int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
        return rolledNumber;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class DiceRoller
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.resultForEachRoll
           );
        }
        public void PrintResults()
        {
            foreach(int Rez in resultForEachRoll)
            {
                Console.WriteLine(Rez);
            }
        }
        public void ConsolePrintResults()
        {
            foreach (int Rez in resultForEachRoll)
            {  
                ConsoleLogger consollogger = new ConsoleLogger();
                consollogger.Log(Rez.ToString());

            }
        }
        public void FilePrintResults(string FilePath)
        {
            foreach (int Rez in resultForEachRoll)
            {
                FileLogger filelogger = new FileLogger(FilePath);
                filelogger.Log(Rez.ToString());   
            }
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
    }
}
